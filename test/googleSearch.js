import {Builder, By, Key, until} from 'selenium-webdriver';
import * as chromedriver from 'chromedriver';

import { expect } from 'chai';
import "chai/register-should";

import * as fs from 'fs';
import {mkdirp} from 'mkdirp';

describe('Test the behavior of Google Search', function(){
    this.timeout(60000);

    let driver;

    before(function(){
        
        var capabilities = {
            'browserName' : 'chrome'
        }
        driver = new Builder().withCapabilities(capabilities).build();
        driver.get('http://www.google.com/ncr');
        return driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    });

    it('should display the search-input as window-title.',(done)=>{
        driver.getTitle().then(function(title){
            expect(title).equals('webdriver - Google Search');
            done();
        });
    });

    afterEach(function(){
        // take a screenshot after each test

        const screenShotDest = 'test/screenshots';

        mkdirp(screenShotDest, function (err) {
            if (err){
               throw error;
            }
        });

        const pngFilePath = `${screenShotDest}/${this.currentTest.state}-${this.currentTest.title.replace(/\W/g, '-')}.png`;        
        driver.takeScreenshot().then((data) => {
            fs.writeFile(pngFilePath, data.replace(/^data:image\/png;base64,/,''), 'base64', function(err) {
                if(err) throw err;
            });
        })
    });

    after(()=>{
        driver.quit();
    });
});