# selenium-playground

Demonstration of selenium+mocha for testing Web-based UI-Frontends.

[test/googleSearch.js](test/googleSearch.js) features a simple test, that shows how to 

1) navigate to google

2) simulate typing "webdriver" as keyword

3) simulate pressing the _ENTER_-key to start the search

4) test wether the window-title  contains the searched keyword (i.e. equals "webdriver - Google Search")

5) take a screenshot after each test for later inspection


## Run Demo
To install all dependencies, you can type
```
yarn
```
To run the example, type 
```
yarn test
```